<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|


Route::get('/', function () {
    return view('welcome');
});*/

Route::get('/', 'HomeController@home');
Route::get('/register', 'AuthController@isidata');
Route::post('/kirim', 'AuthController@send');

Route::get('/master', function() {
    return view('temp.master');
});

route::get('/items', function() {
    return view('items.index');
});

route::get('/items/create', function() {
    return view('items.create');
});

 /* tugas '/table' */
 route::get('/tab/table', function() {
    return view('tab.table');
});

  /* tugas '/data-tables.*/
  route::get('/tab/data-table', function() {
    return view('tab.data-table');
});

/* crud cast
route::get('/cast/create/','CastController@create');
route::post('/cast', 'CastController@store');
route::get('/cast', 'CastController@index');
route::get('/cast/{cast_id}', 'CastController@show');
route::get('/cast/{cast_id}/edit', 'CastController@edit');
route::put('/cast/{cast_id}', 'CastController@update');
route::delete('/cast/{cast_id}', 'CastController@destory');*/

//CRUD RESOURCE 

route::resource('cast', 'CastController');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
