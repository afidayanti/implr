<?php
    
      namespace App\Http\Controllers;

    use Illuminate\Http\Request;
    use Illuminate\Support\Facades\DB;
    use App\cast;
  //use Auth;

    class CastController extends Controller
    {
        
    public function __construct() {
      $this->middleware('auth');
    }
    public function create()
    {
    return view('cast.create');
    }

    public function store(Request $request)
    {
    $request->validate([
        'nama' => 'required|unique:cast,nama|max:255',
        'umur' => 'required',
        'bio' => 'required', 
    ],

    [
        'nama.required' => 'nama harus diisi',
        'umur.required' => 'umur harus diisi',
        'bio.required' => 'bio harus diisi',
    ]);

    //crud biasa
    //DB::table('cast')->insert(
     //   [
     //  'nama' => $request['nama'], 
     //  'umur' => $request['umur'], 
     //  'bio' => $request['bio']
      //  ]
    //);

    //pake manual
   // $cast = new cast;
   // $cast->nama = $request['nama'];
   // $cast->umur = $request['umur'];
   // $cast->bio = $request['bio'];
   // $cast->save();

    //cara pake m.a
    $cast = cast::create([
    'nama' => $request['nama'], 
    'umur' => $request['umur'], 
    'bio' => $request['bio'], 
  // 'user_id' => Auth::id()
    ]);

    return redirect('/cast');

    }
 
    public function index()
    {
      //  $cast = DB::table('cast')->get();
    $cast = cast::all();

       return view('cast.index', compact('cast'));
    }

    public function show($id)
    {
        //$cast = DB::table('cast')->where('id', $id)->first();
        $cast = cast::find($id);
        
        return view('cast.show', compact('cast'));
    }

    public function edit($id)
    {
       // $cast = DB::table('cast')->where('id', $id)->first();
       $cast = cast::find($id);
       return view('cast.edit', compact('cast'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
       'nama' => 'required|unique:cast,nama|max:255',
       'umur' => 'required',
       'bio' => 'required',
        ],
     
       [
       'nama.required' => 'nama harus diisi',
       'umur.required' => 'umur harus diisi',
       'bio.required' => 'bio harus diisi',
        ]);
     
      //  DB::table('cast')
       //   ->where('id', $id)
       //   ->update(
        //       [
        //    'nama' => $request['nama'],
        //    'umur' => $request['umur'],
        //    'bio' => $request['bio'],
        //]
      // );
    $update = cast::where('id', $id)->update([
        'nama' => $request['nama'],
        'umur' => $request['umur'],
        'bio' => $request['bio']
    ]);
        return redirect('/cast');
       
    }

  public function destroy($id)
        {
              // DB::table('cast')->where('id', '=', $id)->delete();
              cast::destroy($id); 
              return redirect('/cast');
           }
 
    } 