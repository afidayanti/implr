<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
   public function isidata()
    {
        return view('register.form');
    }

    public function send(Request $request)
    {
        $namaawal = $request["namaawal"];
        $namaakhir = $request["namaakhir"];

        return view('register/home', compact('namaawal', 'namaakhir'));
    }
}
