@extends('temp.master')
@section('title')
    <h2> tampil data</h2>
@endsection

@section('conten')

<div class="container-fluid">
    <div class="card card-default">
        <div class="container">
            <a href="{{ route('cast.create') }}" class="btn btn-success btn-sm my-2">Tambah</a>
            <table class="table">
                <thead class="thead-light">
                <tr>
                    <th scope="col">No</th>
                    <th scope="col">Nama</th>
                    <th scope="col">Umur</th>
                    <th scope="col">Bio</th> 
                    <th scope="col">Aksi</th>
                </tr>
                </thead>
                <tbody>
            @forelse ($cast as $key => $cast)
            <tr>
                <td>{{$key + 1}}</td>
                <td>{{$cast->nama}}</td>
                <td>{{$cast->umur}}</td>
                <td>{{$cast->bio}}</td>
                <td>
                    <form action="/cast/{{$cast->id}}" method="POST">
                    <a href="{{ route('cast.show', ['cast' => $cast->id])}}" class="btn btn-primary btn-sm">Detail</a> 
                    <a href="{{ route('cast.edit', ['cast' => $cast->id])}}" class="btn btn-primary btn-sm">edit</a> 
                    
                        @csrf
                        @method('delete')
                            <input type="submit"class="btn btn-danger btn-sm"  value="delete">
                    </form>
                </td>
            </tr>
            @empty
                <tr>
                    <td>kosong data</td>
                </tr>
            @endforelse
                </tbody>
            </table>
        </div>
    </div>
</div>


@endsection