@extends('temp.master')
@section('title')
    <h2> halaman data</h2>
@endsection

@section('conten')
<div class="container-fluid">
    <h2>Tambah Data</h2>
    <div class="card card-default">
      <div class="card-header">
        <h3 class="card-title">Tambah data Cast</h3>
        <div class="card-tools">
          <button type="button" class="btn btn-tool" data-card-widget="collapse">
            <i class="fas fa-minus"></i>
          </button>
          <button type="button" class="btn btn-tool" data-card-widget="remove">
            <i class="fas fa-times"></i>
          </button>
        </div>
      </div>  
      <div class="container">
            <form action="/cast" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                    <label>Nama</label>
                    <input type="text" class="form-control" name="nama" placeholder="Masukkan nama kamu">
                    @error('nama')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label>Umur</label>
                    <input type="text" class="form-control" name="umur" placeholder="Masukkan umur kamu">
                    @error('umur')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label>Bio</label>
                    <textarea name="bio" class="form-control"  cols="30" rols="10" placeholder="ceritakan tentang  kamu"></textarea>
                    @error('bio')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <button type="submit" class="btn btn-primary">Tambah</button>
            </form> 
        </div>
    </div>
</div>
@endsection
