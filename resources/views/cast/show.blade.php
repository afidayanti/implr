@extends('temp.master')
@section('title')
   detail data {{$cast->nama}}
@endsection

@section('conten')
<div class="container-fluid">
    <div class="card card-default">
      <div class="card-header">
        <h3 class="card-title">detail data Cast</h3>
        <div class="card-tools">
          <button type="button" class="btn btn-tool" data-card-widget="collapse">
            <i class="fas fa-minus"></i>
          </button>
          <button type="button" class="btn btn-tool" data-card-widget="remove">
            <i class="fas fa-times"></i>
          </button>
        </div>
      </div>  
      <div class="container">
          <h1>{{$cast->nama}}</h1>
          <h2>{{$cast->umur}}</h2>
          <h3>{{$cast->bio}}</h3>
      </div>
    </div>
</div>
    @endsection