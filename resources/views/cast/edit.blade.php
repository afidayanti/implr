@extends('temp.master')
@section('title')
edit data {{$cast->nama}}
@endsection

@section('conten')
<div class="container-fluid">
    <h2>Tambah Data</h2>
    <div class="card card-default">
      <div class="card-header">
        <h3 class="card-title">edit data Cast</h3>
        <div class="card-tools">
          <button type="button" class="btn btn-tool" data-card-widget="collapse">
            <i class="fas fa-minus"></i>
          </button>
          <button type="button" class="btn btn-tool" data-card-widget="remove">
            <i class="fas fa-times"></i>
          </button>
        </div>
      </div>  
      <div class="container">
            <form action="/cast/{{$cast->id}}" method="POST">
                @csrf
                @method('put')
                <div class="form-group">
                    <label for="title">Nama</label>
                    <input type="text" value="{{$cast->nama}}" class="form-control" name="nama">
                    @error('nama')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="body">Umur</label>
                    <input type="text" value="{{$cast->umur}}" class="form-control" name="umur">
                    @error('umur')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="body">Bio</label><br>
                    <textarea name="bio"  cols="30" rols="10">{{$cast->bio}} </textarea>
                    @error('bio')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div> 
                <button type="submit" class="btn btn-primary">update</button>
            </form> 
        </div>
    </div>
</div>
@endsection
